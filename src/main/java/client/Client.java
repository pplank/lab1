package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import cli.Command;
import cli.Shell;
import util.CommandString;
import util.Config;

public class Client implements IClientCli, Runnable {

	private String componentName;
	private Config config;

	private InputStream userRequestStream;
	private PrintStream userResponseStream;
	private Shell shell;

	private Socket socket = null;
	BufferedReader serverReader;
	PrintWriter serverWriter;

	/**
	 * @param componentName
	 *            the name of the component - represented in the prompt
	 * @param config
	 *            the configuration to use
	 * @param userRequestStream
	 *            the input stream to read user input from
	 * @param userResponseStream
	 *            the output stream to write the console output to
	 */
	public Client(String componentName, Config config,
			InputStream userRequestStream, PrintStream userResponseStream) {
		this.componentName = componentName;
		this.config = config;
		this.userRequestStream = userRequestStream;
		this.userResponseStream = userResponseStream;

		// Create and register new Shell
		shell = new Shell(componentName, userRequestStream, userResponseStream);
		shell.register(this);
	}

	@Override
	public void run() {

		new Thread(shell).start();

		System.out.println("Up and running, waiting for commands!");
		try {
			socket = new Socket(config.getString("controller.host"),
					config.getInt("controller.tcp.port"));
			// create a reader to retrieve messages send by the server
			serverReader = new BufferedReader(
					new InputStreamReader(socket.getInputStream()));
			// create a writer to send messages to the server
			serverWriter = new PrintWriter(
					socket.getOutputStream(), true);
		} catch (UnknownHostException e) {
			System.out.println("Cannot connect to server: " + e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getClass().getSimpleName() + ": "
					+ e.getMessage());
		}
	}

	@Command
	@Override
	public String login(String username, String password) throws IOException {
		String message = CommandString.CMD_LOGIN + " " + username + " " + password + "\n";
		return handleCommandAndResponse(message);
	}

	@Command
	@Override
	public String logout() throws IOException {
		String message = CommandString.CMD_LOGOUT + "\n";
		return handleCommandAndResponse(message);
	}

	@Command
	@Override
	public String credits() throws IOException {
		String message = CommandString.CMD_CREDITS + "\n";
		return handleCommandAndResponse(message);
	}

	@Command
	@Override
	public String buy(long credits) throws IOException {
		String message = CommandString.CMD_BUY + " " + credits + "\n";
		return handleCommandAndResponse(message);
	}

	@Command
	@Override
	public String list() throws IOException {
		String message = CommandString.CMD_LIST + "\n";
		return handleCommandAndResponse(message);
	}

	@Command
	@Override
	public String compute(String term) throws IOException {
		String message = CommandString.CMD_COMPUTE + " " + term + "\n";
		System.out.println(message);
		return handleCommandAndResponse(message);
	}

	@Command
	@Override
	public String exit() throws IOException {

		// Afterwards stop the Shell from listening for commands
		shell.close();

		// Close Reader/Writer
		serverWriter.close();
		serverReader.close();

		// Close network connections
		if (socket != null && !socket.isClosed())
			try {
				socket.close();
			} catch (IOException e) {
				// Ignored because we cannot handle it
			}

		return "Shut down completed! Goodbye!";
	}

	public String handleCommandAndResponse(String message) {
		try{
			serverWriter.printf(message);
			serverWriter.flush();

			String response;

			if((response = serverReader.readLine()) != null) {
				return response;
			} else {
				return null;
			}
		} catch(IOException e) {
			try {
				this.exit();
			} catch(IOException e2) {
				// Ignored because we cannot handle it
			}
			return CommandString.CMD_ERROR + ", connection to server has been aborted, closing application";
		}
	}

	/**
	 * @param args
	 *            the first argument is the name of the {@link Client} component
	 */
	public static void main(String[] args) {
		Client client = new Client(args[0], new Config("client"), System.in,
				System.out);
		client.run();
	}

	// --- Commands needed for Lab 2. Please note that you do not have to
	// implement them for the first submission. ---

	@Override
	public String authenticate(String username) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

}
