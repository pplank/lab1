package util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Class for handling formatting of date in concurrent environment
 *
 * @author Pascal Plank, 12 25 804
 *
 */

public class DateFormatter {

	private final static String format = "yyyyMMdd_HHmmss.SSS";

	private static final ThreadLocal<DateFormat> dateformat = new ThreadLocal<DateFormat>(){
		@Override
		protected DateFormat initialValue() {
			return new SimpleDateFormat(format);
		}
	};
	
	public String formatDate(Date date) {
	    return dateformat.get().format(date);
	}
}