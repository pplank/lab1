package util;

public class CommandString {
	public final static String CMD_LOGIN = "!login";
	public final static String CMD_LOGOUT = "!logout";
	public final static String CMD_CREDITS = "!credits";
	public final static String CMD_BUY = "!buy";
	public final static String CMD_LIST = "!list";
	public final static String CMD_COMPUTE = "!compute";
	public final static String CMD_ERROR = "!error";
	public final static String CMD_ALIVE = "!alive";
}
