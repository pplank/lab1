package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.channels.ClosedByInterruptException;

import util.CommandString;
import model.RemoteClient;
import model.RemoteNode;

/**
 * Class for handling a specific dispatched client-connection in a separate thread
 *
 * @author Pascal Plank, 12 25 804
 *
 */
public class ClientConnectionHandler implements Runnable {

	private Socket socket;
	private RemoteClient verifiedClient = null;

	public ClientConnectionHandler(Socket socket) {
		this.socket = socket;
	}

	public void run() {
		BufferedReader reader = null;
		PrintWriter writer = null;

		try {
			// prepare the input reader for the socket
			reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			// prepare the writer for responding to clients requests
			writer = new PrintWriter(socket.getOutputStream(), true);

			// read client requests
			String request;
			while ((request = reader.readLine()) != null) {

				if(verifiedClient != null) {
					System.out.println(verifiedClient.getName() + "> " + request);
				} else {
					System.out.println("Noname> " + request);
				}

				String[] parts = request.split("\\s");

				String response = "";

				if (request.startsWith(CommandString.CMD_LOGIN)) {

					response = handleLogin(parts);

				} else if (request.startsWith(CommandString.CMD_LOGOUT)) {
					response = handleLogout(parts);

				} else if (request.startsWith(CommandString.CMD_CREDITS)) {
					response = handleCredits(parts);

				} else if (request.startsWith(CommandString.CMD_BUY)) {
					response = handleBuying(parts);

				} else if (request.startsWith(CommandString.CMD_LIST)) {
					response = handleList(parts);

				} else if (request.startsWith(CommandString.CMD_COMPUTE)) {
					response = handleCompute(parts);

				} else {
					response = CommandString.CMD_ERROR + ", command is not supported";
				}

				//System.out.println(response);
				writer.println(response);
				writer.flush();
			} 
		} catch (ClosedByInterruptException e) {
			String name = "";
			if(verifiedClient != null) {
				name = verifiedClient.getName();
			} else {
				name = "noname";
			}
			System.out.println("ClientConnectionHandler for " + name + " closed");
		} catch(IOException e) {
			System.err
			.println("Error occurred while waiting for/communicating with client: "
					+ e.getMessage());

			if(verifiedClient != null) {
				verifiedClient.setOffline();
			}
		} finally {
			if (socket != null && !socket.isClosed()) {
				try {
					socket.close();
				} catch (IOException e) {
					// Ignored because we cannot handle it
				}
			}

			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					// Ignored because we cannot handle it
				}
			}

			if (writer != null) writer.close();
		}
	}

	/**
	 * Handles the login for a connected client and sets the 
	 * RemoteClient-Element in Cloud-Controller to online
	 * @param parts client message, first element is !login
	 * @return response
	 */
	public String handleLogin(String[] parts) {
		if(parts.length != 3) {
			return CommandString.CMD_ERROR + " provided message does not fit the expected format: "
					+ "!login <client-name> <password>";
		}

		String loginName = parts[1];
		String loginPassword = parts[2];
		String response = "";

		RemoteClient temp = null;

		// search for loginName
		for(RemoteClient c : CloudController.remoteClients) {
			if(c.getName().equals(loginName)){
				temp = c;
				break;
			}
		}

		// check if connected user from this connection is logged in
		if(loggedIn()) {
			return CommandString.CMD_ERROR + ", " + "You are already loggedin! For new login, logout out first!";
		}

		// check if user from provided login-command is already logged in
		if(temp != null && temp.isOnline()) {
			return CommandString.CMD_ERROR + ", " + temp.getName() + " is already loggedin!";
		}

		// verify password
		if(temp != null && temp.getPassword().equals(loginPassword)) {
			response = "Successfully logged in as " + temp.getName() + ".";
			verifiedClient = temp;
			verifiedClient.setOnline();
		} else {
			response = "Wrong username or password.";
		}
		return response;
	}

	/**
	 * Handles the logout for a logged-in client and sets the 
	 * RemoteClient-Element in Cloud-Controller to offline
	 * @param parts client message, first element is !logout
	 * @return response
	 */
	public String handleLogout(String[] parts) {
		if(parts.length != 1) {
			return CommandString.CMD_ERROR + " provided message does not fit the expected format: "
					+ "!logout, no parameters!";
		}

		if(!loggedIn()) {
			return CommandString.CMD_ERROR + ", Logout only possible while being logged in!";
		}

		verifiedClient.setOffline();
		verifiedClient = null;
		return "Successfully logged out.";
	}

	/**
	 * Handles credit-request for a logged-in client
	 * @param parts client message, first element is !credits
	 * @return response
	 */
	public String handleCredits(String[] parts) {
		if(parts.length != 1) {
			return CommandString.CMD_ERROR + " provided message does not fit the expected format: "
					+ "!credits, no parameters!";
		}

		if(!loggedIn()) {
			return CommandString.CMD_ERROR + ", Credits can only be transmitted after login!";
		}

		return "You have " + verifiedClient.getCredits() + " credits left.";
	}

	/**
	 * Handles credit-purchases for a logged-in client
	 * @param parts client message, first element is !buy
	 * @return response
	 */
	public String handleBuying(String[] parts) {
		if(parts.length != 2) {
			return CommandString.CMD_ERROR + " provided message does not fit the expected format: "
					+ "!buy <credits to buy>";
		}

		if(!loggedIn()) {
			return CommandString.CMD_ERROR + ", Credits can only be transmitted after login!";
		}

		int amount = 0;
		try {
			amount = Integer.valueOf(parts[1]);
		} catch(NumberFormatException e) {
			return CommandString.CMD_ERROR + ", Credits could not be parsed from command!";
		}

		verifiedClient.buyCredits(amount);
		return "You now have " + verifiedClient.getCredits() + " credits.";
	}


	/**
	 * Handles list-request for a logged-in client
	 * @param parts client message, first element is !list
	 * @return response
	 */
	public String handleList(String[] parts) {
		if(parts.length != 1) {
			return CommandString.CMD_ERROR + " provided message does not fit the expected format: "
					+ "!list, no parameters!";
		}

		if(!loggedIn()) {
			return CommandString.CMD_ERROR + ", List of operations can only be transmitted after login!";
		}

		String operations = "";
		for(RemoteNode r : CloudController.remoteNodes) {
			if(!r.isOnline()) continue;

			String currOps = r.getOperations();

			for(int i = 0; i < currOps.length(); i++) {
				if(operations.indexOf(currOps.charAt(i)) == -1) {
					operations += currOps.charAt(i);
				}
			}
		}
		String response = "";
		if(!operations.isEmpty()) {
			response = operations;
		} else {
			response = "No operations available";
		}
		return response;
	}


	/**
	 * Handles compute-request for a logged-in client
	 * @param parts client message, first element is !compute
	 * @return response
	 */
	public String handleCompute(String[] parts) {
		if(parts.length < 4 || (parts.length % 2) != 0) {
			return CommandString.CMD_ERROR + " provided message does not fit the expected format: "
					+ "!compute <term>, <term> has to be a valid mathematical term containing no "
					+ "other arithmetic operation than the command !list returns";
		}

		if(!loggedIn()) {
			return CommandString.CMD_ERROR + ", Computations can only be performed after login!";
		}
		
		if(verifiedClient.getCredits() < 50) return CommandString.CMD_ERROR + ", not enough credits to process calculation";
		// first calculation
		String input = parts[1].trim() + " " + parts[2].trim() + " " + parts[3].trim();
		
		RemoteNode node = selectNode(parts[2].trim());
		
		if(node == null || !node.isOnline()) {
			return CommandString.CMD_ERROR + ", operation <" + parts[2].trim() + "> is not supported.";
		}
		
		String response = computeCalculationViaNode(input, node);
		
		if (response.startsWith(CommandString.CMD_ERROR)){
			return response;
		} else {
			node.increaseUsage(response.trim().length());
		}
		verifiedClient.decrementCredits(1);
		
		for(int i = 4; i < parts.length; i=i+2) {
			if(verifiedClient.getCredits() < 50) {
				return CommandString.CMD_ERROR + ", not enough credits to process calculation, "
						+ "interim result up to '" + parts[i].trim() + " " 
						+ parts[i+1].trim() + "' is " + response;
			}
			
			RemoteNode node2 = selectNode(parts[i].trim());
			
			if(node2 == null || !node2.isOnline()) {
				return CommandString.CMD_ERROR + ", operation <" + parts[i].trim() + "> is not supported. "
						+ "Interim result up to '" + parts[i].trim() + " " 
						+ parts[i+1].trim() + "' is " + response;
			}
			
			response = computeCalculationViaNode(response + " " + parts[i].trim() + " " + parts[i+1].trim(), node2);
			if(response.startsWith(CommandString.CMD_ERROR)) {
				return response;
			} else {
				node2.increaseUsage(response.trim().length());
			}
			
			verifiedClient.decrementCredits(1);
		}
		
		return response;
	}

	

	
	/**
	 * Calculates the given input-calculation via sending it to a node
	 * @param input arithmetic-expression of form: <var 1> <operation> <var2>
	 * @return result of given calculation
	 */
	public String computeCalculationViaNode(String input, RemoteNode node) {
		String result = "";
		Socket socket = null;
		
		try {
			/*
			 * create a new tcp socket at specified host and port for transmitting calculation
			 */
			
			socket = new Socket(node.getAddress(), node.getTcpPort());
			// create a reader to retrieve messages send by the node
			BufferedReader serverReader = new BufferedReader(
					new InputStreamReader(socket.getInputStream()));
			// create a writer to send messages to the server
			PrintWriter serverWriter = new PrintWriter(
					socket.getOutputStream(), true);

			// write provided calculation to the socket (therefore to the node)
			serverWriter.println(input);
			// read server response and write it to console
			result = serverReader.readLine();

		} catch (UnknownHostException e) {
			result = CommandString.CMD_ERROR + ", could not connect to selected node, try again: " + e.getMessage();
		} catch (IOException e) {
			result = CommandString.CMD_ERROR + ", IO-Exception when communicating with node, try again: " + e.getMessage();
		} finally {
			if (socket != null && !socket.isClosed())
				try {
					socket.close();
				} catch (IOException e) {
					// Ignored because we cannot handle it
				}
		}

		return result;
	}
	
	/**
	 * Selects the online node with the lowest usage that supports the given operation
	 * @param operation operation that the node has to be able to perform
	 * @return online-node with the lowest usage and support for given operation, null if 
	 * no such node was found 
	 */
	public RemoteNode selectNode(String operation) {
		RemoteNode node = null;
		
		int lowestUsage = Integer.MAX_VALUE;
		for(RemoteNode n : CloudController.remoteNodes) {
			if(n.getOperations().contains(operation) && n.isOnline()) {
				if(n.getUsage() < lowestUsage) {
					node = n;
					lowestUsage = n.getUsage();
				}
			}
		}
		
		return node;
	}
	
	
	/**
	 * Checks whether the current user is logged in
	 * (= user is verified and online)
	 * @return true if user is logged in
	 */
	private boolean loggedIn() {
		if(verifiedClient != null && verifiedClient.isOnline()) {
			return true;
		} else {
			return false;
		}
	}
}