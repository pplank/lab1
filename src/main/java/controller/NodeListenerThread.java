package controller;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.sql.Timestamp;

import util.CommandString;
import model.RemoteNode;

public class NodeListenerThread extends Thread {
	private DatagramSocket datagramSocket;

	public NodeListenerThread(DatagramSocket datagramSocket) {
		this.datagramSocket = datagramSocket;
	}
	
	public void run() {

		byte[] buffer;
		DatagramPacket packet;
		try {
			while (true) {
				buffer = new byte[1024];
				// create a datagram packet of specified length (buffer.length)
				/*
				 * Keep in mind that: in UDP, packet delivery is not
				 * guaranteed,and the order of the delivery/processing is not
				 * guaranteed
				 */
				packet = new DatagramPacket(buffer, buffer.length);

				// wait for incoming packets from client
				datagramSocket.receive(packet);
				// get the data from the packet
				String request = new String(packet.getData());

				// check if request has the correct format:
				// !alive <TCPPort> <supported operations>
				String[] parts = request.split("\\s");

				if (parts.length == 3) {
					if (request.startsWith(CommandString.CMD_ALIVE)) {
						InetAddress address = packet.getAddress();
						int tcpPort = 0;
						String operations = parts[2].trim();
						try {	
							tcpPort = Integer.parseInt(parts[1]);
							String id = address.toString() + tcpPort + operations;
							
							RemoteNode node = null;
							for(RemoteNode r : CloudController.remoteNodes) {
								if(id.equals(r.getId())) {
									node = r;
									node.setOnline();
									node.setLastAliveBeacon(new Timestamp(System.currentTimeMillis()));
									break;
								}
							}
							
							if(node == null) {
								CloudController.remoteNodes.add(new RemoteNode(address, tcpPort, operations));
							}
							
						} catch(NumberFormatException e) {
							System.out.println("Could not read TCP-Port from alive-packet from " + address);
						}
					}
				}
			}

		} catch (IOException e) {
			System.err
					.println("Error occurred while waiting for/handling packets: "
							+ e.getMessage());
		} finally {
			if (datagramSocket != null && !datagramSocket.isClosed())
				datagramSocket.close();
		}

	}
}
