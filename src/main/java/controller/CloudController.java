package controller;

import util.Config;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import model.RemoteClient;
import model.RemoteNode;
import cli.Command;
import cli.Shell;

public class CloudController implements ICloudControllerCli, Runnable {

	private String componentName;
	private Config config;
	
	private Shell shell;
	private InputStream userRequestStream;
	private PrintStream userResponseStream;
	
	ServerSocket serverSocket;
	public static List<RemoteClient> remoteClients = new ArrayList<RemoteClient>();
	public static List<RemoteNode> remoteNodes = new ArrayList<RemoteNode>();
	ClientConnectionDispatcher cDispatcher;
	private NodeAliveManager nodeManager;
	private DatagramSocket nodeListenerSocket;
	NodeListenerThread nListener;

	/**
	 * @param componentName
	 *            the name of the component - represented in the prompt
	 * @param config
	 *            the configuration to use
	 * @param userRequestStream
	 *            the input stream to read user input from
	 * @param userResponseStream
	 *            the output stream to write the console output to
	 */
	public CloudController(String componentName, Config config,
			InputStream userRequestStream, PrintStream userResponseStream) {
		this.componentName = componentName;
		this.config = config;
		this.userRequestStream = userRequestStream;
		this.userResponseStream = userResponseStream;
		
		// Create and register new Shell
		shell = new Shell(componentName, userRequestStream, userResponseStream);
		shell.register(this);
	}

	@Override
	public void run() {
		this.retrieveClientData();
		
		new Thread(shell).start();
		

		try {
			// create and start a new UDP Listener for Alive-Packages
			nodeListenerSocket = new DatagramSocket(config.getInt("udp.port"));
			nListener = new NodeListenerThread(nodeListenerSocket);
			nListener.start();
			
			// create and start a new Dispatcher-Thread for Client-Connections
			serverSocket = new ServerSocket(config.getInt("tcp.port"));
			// handle incoming connections from client in a separate thread
			cDispatcher = new ClientConnectionDispatcher(serverSocket);
			cDispatcher.start();
			
			nodeManager = new NodeAliveManager(config.getInt("node.checkPeriod"), config.getInt("node.timeout"));
			nodeManager.run();
		} catch (IOException e) {
			throw new RuntimeException("Cannot listen on TCP/UDP port.", e);
		}
	}

	@Command
	@Override
	public String nodes() throws IOException {
		String message = "";
		
		String status = "";
		for(RemoteNode n : remoteNodes) {
			if(n.isOnline()) {
				status = "online";
			} else {
				status = "offline";
			}
			message = message
					+ String.format("IP: %s", n.getAddress().toString()) 
					+ String.format(" Port: %5d %7s", n.getTcpPort(), status)
					+ String.format(" Usage: %d\n", n.getUsage());
		}
		
		return message;
	}

	@Command
	@Override
	public String users() throws IOException {
		String message = "";
		
		String status = "";
		for(RemoteClient c : remoteClients) {
			if(c.isOnline()) {
				status = "online";
			} else {
				status = "offline";
			}
			message = message
					+ String.format("%-11s", c.getName()).replaceAll(" ", ".") 
					+ String.format(" %7s, Credits: %5d\n", status, c.getCredits());
		}
		
		return message;
	}

	@Command
	@Override
	public String exit() throws IOException {	
		if (serverSocket != null && !serverSocket.isClosed()) {
			try {
				serverSocket.close();
			} catch (IOException e) {
				// Ignored because we cannot handle it
			}
		}
		
		if (nodeListenerSocket != null && !nodeListenerSocket.isClosed()) {
			nodeListenerSocket.close();
		}
		
		// Afterwards stop the Shell from listening for commands
		shell.close();
		
		nodeManager.setShutdown();
		
		return "Cloudcontroller has been shut down.";
	}

	/**
	 * @param args
	 *            the first argument is the name of the {@link CloudController}
	 *            component
	 */
	public static void main(String[] args) {
		CloudController cloudController = new CloudController(args[0],
				new Config("controller"), System.in, System.out);

		cloudController.run();
	}


	/**
	 * Retrieve data about known clients from property-file
	 */
	public void retrieveClientData() {
		Config c = new Config("user");

		Set<String> keys = c.listKeys();

		// Find all unique client-names
		Set<String> users = new HashSet<String>();
		for(String s : keys) {
			users.add(s.substring(0, s.indexOf(".")));
		}

		// create new remotClients
		for(String s : users) {
			RemoteClient newClient = new RemoteClient(s, c.getString(s + ".password"), c.getInt(s + ".credits"));
			CloudController.remoteClients.add(newClient);
		}
	}
}
