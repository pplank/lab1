package controller;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.ClosedByInterruptException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Thread listens for incoming connections on the given socket and 
 * dispatching them to separate threads.
 * 
 * @author Pascal Plank, 12 25 804
 * 
 */
public class ClientConnectionDispatcher extends Thread {

	public static final int CLIENTTHREADPOOL_SIZE = 25;
	private ServerSocket dispatcherSocket;

	public ClientConnectionDispatcher(ServerSocket dispatcherSocket) {
		this.dispatcherSocket = dispatcherSocket;
	}

	public void run() {
		List<Socket> clientSockets = new ArrayList<Socket>();
		ExecutorService executorService = Executors.newFixedThreadPool(CLIENTTHREADPOOL_SIZE);

		System.out.println("Cloudcontroller listening at " 
				+ dispatcherSocket.getInetAddress().getHostAddress() 
				+ ":" + dispatcherSocket.getLocalPort() + " for incoming clients...");
		
		try {
			Socket tempSocket;
			while (true) {
				// wait for Client to connect
				tempSocket = dispatcherSocket.accept();
				clientSockets.add(tempSocket);
				System.out.println("New Client has connected");
				// Handle new connection in separate thread
				executorService.execute(new ClientConnectionHandler(tempSocket));
				
				// Get rid of all socket that closed so far
				// to keep the list at a manageable length
				ListIterator<Socket> socketIter = clientSockets.listIterator();
				while(socketIter.hasNext()) {
					Socket current = socketIter.next();
					if(current.isClosed()) {
						socketIter.remove();
					}
				}
			}
		} catch (ClosedByInterruptException e) {
			System.out.println("ClientDispatcher closed");
		} catch (IOException e) {
			System.err
			.println("Error occurred while waiting for/communicating with client: "
					+ e.getMessage() + ", " + e.getClass());
		} finally {
			if (dispatcherSocket != null && !dispatcherSocket.isClosed()) {
				try {
					dispatcherSocket.close();
				} catch (IOException e) {
					// Ignored because we cannot handle it
				}
			}
			// This will only work while the thread are not blocking on IO-Operations
			executorService.shutdownNow();

			// Close all open ClientSockets to cause IO-Exceptions in threads
			ListIterator<Socket> socketIter = clientSockets.listIterator();
			while(socketIter.hasNext()) {
				Socket current = socketIter.next();
				if (current != null && !current.isClosed()) {
					try {
						current.close();
					} catch (IOException e) {
						// Ignored because we cannot handle it
					}
				}
			}
		}
	}
}
