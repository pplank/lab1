package controller;

import java.sql.Timestamp;
import model.RemoteNode;


/**
 * Thread checks at the given checkPeriod if nodes are alive 
 * (alive = node sent alive package within the last timeout)
 * 
 * @author Pascal Plank, 12 25 804
 * 
 */
public class NodeAliveManager extends Thread {

	private final int checkPeriod;
	private final int timeout;
	private boolean shutdown = false;

	public NodeAliveManager(int checkPeriod, int timeout) {
		this.checkPeriod = checkPeriod;
		this.timeout = timeout;
	}

	public void run() {
		Timestamp currentTime;
		try{
			while(!isShutdown()) {
				Thread.sleep(checkPeriod);
				currentTime = new Timestamp(System.currentTimeMillis());
				
				for(RemoteNode n : CloudController.remoteNodes) {
					if((currentTime.getTime() - n.getLastAliveBeacon().getTime()) > timeout) {
						n.setOffline();
					}
				}
			}
		} catch(InterruptedException e) {

		}
	}
	
	public synchronized void setShutdown() {
		this.shutdown = true;
	}
	
	public synchronized boolean isShutdown() {
		return this.shutdown;
	}
}
