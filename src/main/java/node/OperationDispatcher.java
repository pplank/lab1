package node;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.ClosedByInterruptException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import node.OperationConnectionHandler;

/**
 * Thread listen for incoming connections on the given socket and 
 * dispatching them to separate threads to process the requested arithmetic calculation
 * 
 * @author Pascal Plank, 12 25 804
 * 
 */
public class OperationDispatcher extends Thread {

	public static final int OPERATIONTHREADPOOL_SIZE = 20;
	private ServerSocket dispatcherSocket;
	private final String path;
	private final String componentName;
	
	public OperationDispatcher(ServerSocket dispatcherSocket, String path, String componentName) {
		this.dispatcherSocket = dispatcherSocket;
		this.path = path;
		this.componentName = componentName;
	}

	@Override
	public void run() {
		ExecutorService executorServiceComputations = Executors.newFixedThreadPool(OPERATIONTHREADPOOL_SIZE);
		ExecutorService executorServiceLogging = Executors.newFixedThreadPool(OPERATIONTHREADPOOL_SIZE);
		
		System.out.println("Node listening at " 
				+ dispatcherSocket.getInetAddress().getHostAddress() 
				+ ":" + dispatcherSocket.getLocalPort() + " for incoming operations...");
		
		try {
			Socket tempSocket;
			while (true) {
				// wait for Client to connect
				tempSocket = dispatcherSocket.accept();

				// Handle new connection in separate thread
				executorServiceComputations.execute(new OperationConnectionHandler(tempSocket, path, componentName, executorServiceLogging));
			}
		} catch (ClosedByInterruptException e) {
			System.out.println("OperationDispatcher closed");
		} catch (IOException e) {
			System.err
			.println("Error occurred while waiting for computation-requests: "
					+ e.getMessage() + ", " + e.getClass());
		} finally {
			if (dispatcherSocket != null && !dispatcherSocket.isClosed()) {
				try {
					dispatcherSocket.close();
				} catch (IOException e) {
					// Ignored because we cannot handle it
				}
			}
			// This will only work while the thread are not blocking on IO-Operations
			executorServiceComputations.shutdownNow();
			executorServiceLogging.shutdown();
			// Since the operations don't take long, the operation-threads will end 
			// within a few milliseconds
		}
	}
}
