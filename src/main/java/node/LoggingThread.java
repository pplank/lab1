package node;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Date;

import util.DateFormatter;



public class LoggingThread extends Thread {

	private final String calculation;
	private final String result;
	private final String path;
	private final String componentName;

	public LoggingThread(String calculation, String result, String path, String componentName) {
		this.calculation = calculation;
		this.result = result;
		this.path = path;
		this.componentName = componentName;
	}

	public void run() {
		PrintWriter writer = null;
		try {
			Date d = new Date();
			DateFormatter df = new DateFormatter();

			String filename = path + "/" + df.formatDate(d) + "_" + componentName + ".log";
			
			File file = new File(filename);
			file.getParentFile().mkdirs();
			
			writer = new PrintWriter(file, "UTF-8");
			writer.println(calculation);
			writer.println(result);
			writer.flush();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
}
