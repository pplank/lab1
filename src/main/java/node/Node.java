package node;

import util.Config;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;

import node.OperationDispatcher;
import cli.Command;
import cli.Shell;

public class Node implements INodeCli, Runnable {

	private String componentName;
	private String loggingPath;
	private Config config;
	
	private Shell shell;
	private InputStream userRequestStream;
	private PrintStream userResponseStream;
	
	// Controller-Info
	private InetAddress controllerIP;
	
	// Sending-Info
	private int tcpPort;
	private String operators;
	
	//Dispatcher variables
	private ServerSocket dispatcherSocket;
	private OperationDispatcher opDispatcher;

	private AliveBeaconSender beaconSender;
	/**
	 * @param componentName
	 *            the name of the component - represented in the prompt
	 * @param config
	 *            the configuration to use
	 * @param userRequestStream
	 *            the input stream to read user input from
	 * @param userResponseStream
	 *            the output stream to write the console output to
	 */
	public Node(String componentName, Config config,
			InputStream userRequestStream, PrintStream userResponseStream) {
		this.componentName = componentName;
		this.config = config;
		this.userRequestStream = userRequestStream;
		this.userResponseStream = userResponseStream;
		this.operators = this.config.getString("node.operators");
		tcpPort = this.config.getInt("tcp.port");
		loggingPath = this.config.getString("log.dir");
		
		// Create and register new Shell
		shell = new Shell(componentName, userRequestStream, userResponseStream);
		shell.register(this);
	}

	@Override
	public void run() {
		new Thread(shell).start();
		
		try {
			controllerIP = InetAddress.getByName(config.getString("controller.host"));
			
			// create and start a new Dispatcher-Thread for Client-Connections
			dispatcherSocket = new ServerSocket(tcpPort);
			// handle incoming connections from client in a separate thread
			opDispatcher = new OperationDispatcher(dispatcherSocket, loggingPath, componentName);
			opDispatcher.start();
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// create and start a UDP-Sending Thread for Alive datagrams
		beaconSender = new AliveBeaconSender(config.getInt("controller.udp.port"), config.getInt("node.alive"), tcpPort, operators, controllerIP);
		beaconSender.start();
	}

	@Command
	@Override
	public String exit() throws IOException {
		beaconSender.setShutdown();
		
		if (dispatcherSocket != null && !dispatcherSocket.isClosed()) {
			try {
				dispatcherSocket.close();
			} catch (IOException e) {
				// Ignored because we cannot handle it
			}
		}
		
		shell.close();
		return "Node has been shut down";
	}

	@Override
	public String history(int numberOfRequests) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @param args
	 *            the first argument is the name of the {@link Node} component,
	 *            which also represents the name of the configuration
	 */
	public static void main(String[] args) {
		Node node = new Node(args[0], new Config(args[0]), System.in,
				System.out);
		node.run();
	}

	// --- Commands needed for Lab 2. Please note that you do not have to
	// implement them for the first submission. ---

	@Override
	public String resources() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

}
