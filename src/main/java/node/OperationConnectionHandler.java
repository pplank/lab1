package node;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.channels.ClosedByInterruptException;
import java.util.concurrent.ExecutorService;

import util.CommandString;

/**
 * Class for handling a specific dispatched computation-connection in a separate thread
 *
 * @author Pascal Plank, 12 25 804
 *
 */
public class OperationConnectionHandler extends Thread {

	private Socket socket;
	private final String path;
	private final String componentName;
	private ExecutorService executorServiceLogging;

	public OperationConnectionHandler(Socket socket, String path, String componentName, ExecutorService executorService) {
		this.socket = socket;
		this.path = path;
		this.componentName = componentName;
		this.executorServiceLogging = executorService;
	}

	public void run() {
		BufferedReader reader = null;
		PrintWriter writer = null;

		try {
			// prepare the input reader for the socket
			reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			// prepare the writer for responding to clients requests
			writer = new PrintWriter(socket.getOutputStream(), true);

			// read client requests
			String request;
			while ((request = reader.readLine()) != null) {

				String[] parts = request.split("\\s");
				String response = "";
				
				if(parts.length != 3) {
					response = CommandString.CMD_ERROR + ", incorrect arithemetic expression.";
				} else {
					try {
						int a = Integer.parseInt(parts[0].trim()); 
						int b = Integer.parseInt(parts[2].trim());
						String op = parts[1].trim();

						response = processCalculation(a, b, op);
						
						
						executorServiceLogging.execute(new LoggingThread(request, response, path, componentName));
						
					} catch(NumberFormatException e) {
						response = CommandString.CMD_ERROR + ", could not parse integers.";
					}
				}
				
				//System.out.println(response);
				writer.println(response);
				writer.flush();
			} 
		} catch (ClosedByInterruptException e) {
			System.out.println("OperationConnectionHandler closed due to Interrupt-Exception");
		} catch(IOException e) {
			System.err
			.println("Error occurred while waiting for/communicating with cloudcontroller: "
					+ e.getMessage());
		} finally {
			if (socket != null && !socket.isClosed()) {
				try {
					socket.close();
				} catch (IOException e) {
					// Ignored because we cannot handle it
				}
			}

			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					// Ignored because we cannot handle it
				}
			}

			if (writer != null) writer.close();
		}
	}
	
	private String processCalculation(int a, int b, String op){
		String result = "";
		
		if("+".equals(op)) {
			result = String.valueOf(a + b);
		} else if("-".equals(op)) {
			result = String.valueOf(a - b);
		}else if("*".equals(op)) {
			result = String.valueOf(a * b);
		}else if("/".equals(op)) {
			if(b == 0) {
				result = CommandString.CMD_ERROR + ", division by 0, interim result up to division is " + a;
			} else {
				result = String.valueOf(Math.round((float)a / (float)b));
			}
		} else {
			result = CommandString.CMD_ERROR + ", operation '" + op + "' is not supported";
		}
		return result;
	}
}
