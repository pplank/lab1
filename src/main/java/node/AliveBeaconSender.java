package node;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

import util.CommandString;

public class AliveBeaconSender extends Thread {

	private final int aliveBeaconRate;
	private final int controllerUDPPort;
	private boolean shutdown = false;
	private final int tcpPort;
	private final String operators;
	private final InetAddress controllerIP;
	

	public AliveBeaconSender(int controllerUDPPort, int aliveBeaconRate, int tcpPort, String operators, InetAddress controllerIP) {
		this.controllerUDPPort = controllerUDPPort;
		this.aliveBeaconRate = aliveBeaconRate;
		this.tcpPort = tcpPort;
		this.operators = operators;
		this.controllerIP = controllerIP;
	}
	
	public void run() {

		DatagramSocket socket = null;
		BufferedReader userInputReader = null;

		try {
			// open a new DatagramSocket
			socket = new DatagramSocket();
			// create a new Reader to read user-input from System.in
			userInputReader = new BufferedReader(new InputStreamReader(
					System.in));

			System.out.println("Node is up!");
			

			byte[] buffer;
			DatagramPacket packet;
			while (!this.isShutdown()) {				
				// wait for user-input
				Thread.sleep(aliveBeaconRate);

				// append the name of the client to the user-input
				String message = CommandString.CMD_ALIVE + " " + tcpPort + " " + operators;

				// convert the input String to a byte[]
				buffer = message.getBytes();
				// create the datagram packet with all the necessary information
				// for sending the packet to the server
				packet = new DatagramPacket(buffer, buffer.length,
						controllerIP,
						controllerUDPPort);

				// send request-packet to server
				socket.send(packet);
			}
		} catch (UnknownHostException e) {
			System.out.println("Cannot connect to host: " + e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getClass().getSimpleName() + ": "
					+ e.getMessage());
		} catch (InterruptedException e) {
			System.out.println("wait() has been interrupted: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (socket != null && !socket.isClosed())
				socket.close();

			if (userInputReader != null)
				try {
					userInputReader.close();
				} catch (IOException e) {
					// Ignored because we cannot handle it
				}
		}
	}
	
	public synchronized void setShutdown() {
		this.shutdown = true;
	}
	
	public synchronized boolean isShutdown() {
		return this.shutdown;
	}
}
