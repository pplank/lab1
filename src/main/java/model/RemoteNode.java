package model;

import java.net.InetAddress;
import java.sql.Timestamp;

/**
 * This class represents info about a remote node
 * @author Pascal Plank, 12 25 804
 * 
 * Concurrency hint: it is not possible for two invocations of synchronized methods 
 * on the same object to interleave. When one thread is executing a synchronized 
 * method for an object, all other threads that invoke synchronized methods for the 
 * same object block (suspend execution) until the first thread is done with the object.
 *
 */

public class RemoteNode {
	private int usage = 0;
	private final InetAddress address;
	private final int tcpPort;
	private final String operations;
	private boolean online;
	private final String id;
	private volatile Timestamp lastAliveBeacon;

	public RemoteNode(InetAddress address, int tcpPort, String operations) {
		this.address = address;
		this.tcpPort = tcpPort;
		this.operations = operations;
		this.online = true;
		this.id = address.toString() + tcpPort + operations;
		this.lastAliveBeacon = new Timestamp(System.currentTimeMillis());
	}
	
	public boolean isOnline() {
		return online;
	}

	public synchronized void setOnline() {
		this.online = true;
	}

	public synchronized void setOffline() {
		this.online = false;
	}

	public synchronized int getUsage() {
		return usage;
	}
	
	/**
	 * 
	 * @param times >= 0
	 */
	public synchronized void increaseUsage(int times) {
		this.usage = usage + 50 * times;
	}
	
	public InetAddress getAddress() {
		return address;
	}

	public int getTcpPort() {
		return tcpPort;
	}

	public String getOperations() {
		return operations;
	}

	public Timestamp getLastAliveBeacon() {
		return lastAliveBeacon;
	}

	public void setLastAliveBeacon(Timestamp lastAliveBeacon) {
		this.lastAliveBeacon = lastAliveBeacon;
	}

	public String getId() {
		return id;
	}
}