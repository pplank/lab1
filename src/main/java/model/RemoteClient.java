package model;

/**
 * This class represents info about a remote client
 * @author Pascal Plank, 12 25 804
 * 
 * Concurrency hint: it is not possible for two invocations of synchronized methods 
 * on the same object to interleave. When one thread is executing a synchronized 
 * method for an object, all other threads that invoke synchronized methods for the 
 * same object block (suspend execution) until the first thread is done with the object.
 *
 */

public class RemoteClient {
	private final String name;
	private int credits;
	private final String password;
	private volatile boolean online;

	public RemoteClient(String name, String password, int credits) {
		this.name = name;
		this.password = password;
		this.credits = credits;
		this.online = false;
	}
	
	public boolean isOnline() {
		return online;
	}

	public void setOnline() {
		this.online = true;
	}

	public void setOffline() {
		this.online = false;
	}
	
	public String getName() {
		return name;
	}
	
	public String getPassword() {
		return password;
	}

	public synchronized int getCredits() {
		return credits;
	}
	
	/**
	 * 
	 * @param times >= 0
	 */
	public synchronized void decrementCredits(int times) {
		this.credits = credits - 50 * times;
	}
	
	public synchronized void buyCredits(int amount) {
		this.credits += amount;
	}
}