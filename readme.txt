Reflect about your solution!

Summary:


The shutdown usually causes some intentional error-messages, because there is no way to properly 
interrupt a thread while it's blocking in an IO-call. To set a flag would resolve this issue,
but the application would have to wait for the next package to arrive in order to check 
for the interrupt-flag, which in turn would slow down the shutdown dramatically.

Since the testing-environment neither supports proper scenario independency nor is it possible to
test concurrency in a reliable way (and the provided test-example is incorrect) I had to adjust
my application to be able to at least test some aspects automatically, which made some parts of the
application look rather "ugly", e.g. to avoid static fields to be able to test operators from different
nodes.



Client:
The client runs in a single thread and implements the specified commands via the provided Shell.
The commands are only visible them enter has been pressed, due to it's provided configuration by
the LVA-Leitung/Übungsleiter.



Cloudcontroller:
The Controller reads all specified clients from the property-files and stores them in a static list
for easy access, e.g. for login-validation. The controller starts 3 threads:
1.) a client-dispatcher for waiting for new clients to connect and to hand them down to a new 
thread for handling a specific client
2.) a NodeListener for listening for incomming alive-packages
3.) a NodeManager for testing if a node has exceeded its timeout and managing a static list of nodes
and their properties for easy controller-wide access

The ClientConnectionHandler handles a specific Client-connection dispatched from the 
ClientConnectionDispatcher. It receives all commands and manages the login/logout with the help
of the static list of client retrieved in the startup of the controller. A factory-pattern would
probably looked "prettier", but I wanted to keep the controller simple and not crowded with dozen of
classes, that don't do much. The clientsthreads are managed via a fixed thread pool for gracefull
degration. The creating thrads keep a reference to the socked used in the new thread to cause an
IO-Exception from outside (blocking threads can't be interuppted, see above), which leads to 
error-messages when stutdown is initiated, but accelerates the shutdown immensely. The controller
implements the provided Shell.


Node:
The node used the provided Shell and starts 2 Threads, one for sending the alive-Messaged and one for
dispatching incomming tcp-connections to new threads (OperationConnectionHandler) via a fixed thread 
pool. The OperationConnectionHandler handles a tcp-connection for processing a given calculation and
after the return message has been sent, the threads end. The Dispatcher is shutdown via an 
IO-Exception. The OperationConnectionHandler starts a new Thread for handling the logging. This thread
also runns out within a second or two. The AliveBeaconSender (for sending the alive messages) shutsdown
via a set flag, because it runs in a loop with a small sleeping time, which ensures a fast shutdown.
Note that each Node can and will process all sent calculations with all available operations. It is
the controller that decides via the sent alive packages which nodes to select for a specific 
calculation. Hence, the nodes do not reject calculations when sent to them.


Testing:
-) The contexts of different testing-scenarios are not independent 
(especially when using static fields in classes)
-) Hence, my tests are all in one file
-) Because there are a lot of calculations in a given period of time when using test-scenarios, 
some log-files can be overwritten due to the specified log-format. So there might not be a single 
logfile for every calculation. With manual testing this is very unlikely.
